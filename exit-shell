#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import fcntl
import sys

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class MainWindow(Gtk.Window):
    # Define user commands
    cmd_shutdown = "shutdown -P now"
    cmd_reboot = "shutdown -r now"
    cmd_suspend = "systemctl suspend -i"
    cmd_hibernate = "systemctl hibernate -i"
    cmd_logout = "i3 exit"
    cmd_lock = "i3-lock"

    def __init__(self):
        # Creating a window
        Gtk.Window.__init__(self, title="Exit Shell")
        self.set_border_width(4)
        self.set_default_size(200, 400)
        self.set_resizable(False)
        self.set_keep_above(True)
        self.stick
        self.connect("destroy", Gtk.main_quit)
        self.show()

        # Creating vertical box container for buttons
        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.add(self.box)
        self.box.show()

        # Buttons resources: labels, icons, functions
        self.labels = ("Poweroff", "Reboot", "Suspend",
                       "Hibernate", "Lock", "Exit", "Cancel")

        self.icons = (Gtk.STOCK_STOP, Gtk.STOCK_REFRESH, Gtk.STOCK_FLOPPY,
                      Gtk.STOCK_HARDDISK, Gtk.STOCK_DIALOG_WARNING,
                      Gtk.STOCK_QUIT, Gtk.STOCK_CLOSE)

        self.funcs = (self.shutdown, self.reboot, self.suspend,
                      self.hibernate, self.lock, self.logout, self.cancel)

        # Creating buttons
        for label, icon, func in zip(self.labels, self.icons, self.funcs):
            button = Gtk.Button(label=label, image=Gtk.Image(stock=icon))
            button.set_border_width(4)
            button.connect("clicked", func)
            self.box.pack_start(button, True, True, 0)
            button.show()

    def shutdown(self, widget):
        os.system(self.cmd_shutdown)
        Gtk.main_quit()

    def reboot(self, widget):
        os.system(self.cmd_reboot)
        Gtk.main_quit()

    def suspend(self, widget):
        os.system(self.cmd_suspend)
        Gtk.main_quit()

    def hibernate(self, widget):
        os.system(self.cmd_hibernate)
        Gtk.main_quit()

    def logout(self, widget):
        os.system(self.cmd_logout)
        Gtk.main_quit()

    def lock(self, widget):
        os.system(self.cmd_lock)
        Gtk.main_quit()

    def cancel(self, widget):
        Gtk.main_quit()


def run_once():
    """Locks a script file. So script can have only one instance."""
    global f
    f = open(os.path.realpath(__file__), 'r')
    try:
        fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError:
        print("Script is already running")
        os._exit(0)


if __name__ == "__main__":
    run_once()
    window = MainWindow()
    Gtk.main()
    f.close()
